// Node modules
var gulp 				    = require('gulp')
    express 				= require('express'),
    compression 			= require('compression'),
    livereload 				= require('gulp-livereload'),
    uglify                  = require('gulp-uglify'),
    pump                    = require('pump'),
    runSequence   			= require('run-sequence'),
    concat                  = require('gulp-concat'),
    rename                  = require('gulp-rename'),
    cleanCSS                = require('gulp-clean-css'),
    sourcemaps              = require('gulp-sourcemaps');

// Configuration
var serverport 				= 5000,
    livereloadhost 			= 'localhost',
    livereloadport 			= 35729,
    jsPath                  = 'sources/js';

var runOptions	= {
    isLiveReload: false
};

gulp.task('run_server', function() {

    /**
     * Start express server
     */
    var server = express();
    server.use(compression());
    server.use(express.static('./deploy'));
    server.all('/*', function(req, res) {
        res.sendFile('index.html', { root: 'deploy' });
    });

    server.listen(serverport);

    livereload.listen({
        host: livereloadhost,
        port: livereloadport
    });

    runOptions.isLiveReload = true;
});

gulp.task('minify-css', function() {
    return gulp.src('sources/css/**/*.css')
        .pipe(concat('all.css'))
        .pipe(cleanCSS())
        .pipe(gulp.dest('./deploy/assets'));
});

gulp.task('js', function(){
    return gulp.src([
        jsPath + '/thirdparty/angular.min.js',
        jsPath + '/thirdparty/angular-ui-router.min.js',
        jsPath + '/app.js',
        jsPath + '/services/**/*.js',
        jsPath + '/filters/**/*.js',
        jsPath + '/directives/**/*.js',
        jsPath + '/controllers/**/*.js'])
        .pipe(concat('all.js'))
        .pipe(gulp.dest('./deploy/assets'));
});

gulp.task('server', function (callback) {
    runSequence('minify-css', 'run_server', 'js', callback);
});
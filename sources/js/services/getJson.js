testApp.service('getJson', [
		'$q',
		'$http',

		getJson
	]
);

function getJson($q, $http) {
	'use strict';
	/*jshint validthis: true */

	// private
	var promises = {
		getJson: null
	};

	var defaultOptions = {
		needReload:         false
	};

	var get                 = function (options) {

		options = options || defaultOptions;

		if (promises.getJson && options.needReload === false) {
			return promises.getJson;
		}

		var defer = $q.defer();

		$http.get('assets/tasks.json').success(function(data) {
			defer.resolve(data);
		});

		promises.getJson = defer.promise;

		return defer.promise;
	};

	// public
	this.get          = get;

}
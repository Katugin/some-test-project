testApp.controller('dataController' , [
		'getJson',
		'$scope',
		'$stateParams',
		'$location',
		'$http',

		dataController
	]);

function dataController(getJson, $scope, $stateParams, $location, $http) {
	'use strict';

	const active = 'active';

	var isValid             = function (element) {
		return element.obj_status === active;
	};

	var overrideData        = function (data) {
		var result = [];
		for (var i = 0; i < data.length; i++) {
			if (isValid(data[i]) === true)
				result.push(data[i]);
		}
		return result;
	};

	var init                = function () {
		getJson.get().then(function (data) {
			$scope.data = overrideData(data);
			$scope.task = $scope.data[$stateParams.id];
		});
	};

	init();

	$scope.isEdited         = {};

	$scope.edit             = function (type) {
		$scope.isEdited[type] = true;
	};

	$scope.save             = function () {

		var url = '/',
			data = $scope.task;

		$http.put(url, data)
			.success(function (data, status, headers, config) {
				console.log($scope.task);
				console.warn('success');
			})
			.error(function (data, status, header, config) {
				console.warn('fail');
			});
	};

	$scope.back             = function () {
		$location.path('/description');
	}

}
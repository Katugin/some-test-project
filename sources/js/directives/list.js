
angular
	.module('testApp')
	.directive('list' , ['$location', list]);

function list ($location) {
	'use strict';
	return {
		restrict:		'E',
		templateUrl:	'tpl/data/table.html',
		scope: {
			content:        '='
		},
		controller: function ($scope) {
			$scope.openTask = function (task) {
				$location.path('/data/' + task.id);
			}
		}
	};
}
var testApp = angular.module('testApp',
	[
		'ui.router'
	]
);

testApp.config(function($stateProvider, $urlRouterProvider) {

	$urlRouterProvider.otherwise('/data');

	$stateProvider

		.state('data', {
			url: '/data',
			templateUrl: 'tpl/data-list.html',
			controller: dataController
		})

		.state('data.description', {
			url: '/:id',
			templateUrl: 'tpl/description.html',
			controller: dataController
		})

});